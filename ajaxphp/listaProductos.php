<?php
require_once '../clasesGenericas/ConectorBD.php';
require_once '../clases/Producto.php';
$parametro = $_REQUEST['Enviado'];
$productos = Producto::getLista(" * from producto where disponible <> '0' and nombre ilike '%$parametro%'");

$arreglo = array();

foreach ($productos as  $key=>$datos){
    $arreglo[$key]['value'] = $datos['nombre'];
    $arreglo[$key]['data'] = array($datos['id'],$datos['stock'], $datos['stockminimo'], $datos['stockmaximo'], $datos['stockinicial'], $datos['valorunitario'], $datos['iva'], $datos['idempresa'], $datos['disponible']);
}
echo json_encode(["suggestions" => $arreglo ]);

die();