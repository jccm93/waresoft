<?php
require_once '../clasesGenericas/ConectorBD.php';
require_once '../clases/Producto.php';
require_once '../clases/Persona.php';

if(isset($_POST['opcion']) && $_POST['opcion']=='1'){
    session_start();
    
    $resultado=Producto::getLista('  id, nombre, stock, stockminimo from producto where stock<=stockminimo ');

    $json=null;

    foreach ($resultado as $key => $value) {
        $json[] = array(
            "id" => $value['id'],
            "nombre" => $value['nombre'],
            "stock" => $value['stock'],
            "stockminimo" => $value['stockminimo']
        );
    }

    echo json_encode($json);
}