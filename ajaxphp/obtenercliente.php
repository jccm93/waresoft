<?php
require_once '../clasesGenericas/ConectorBD.php';
require_once '../clases/Persona.php';
$cliente = new Persona($_POST['identificacion']);
$json='';

    $json = array(
        "identificacion" => $cliente->getIdentificacion(),
        "nombre" => $cliente->getNombres(),
        "apellido" => $cliente->getApellidos(),
        "telefono" => $cliente->getTelefono(),
        "tipo" => $cliente->getTipo()
    );



echo json_encode($json);