<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ciudad
 *
 * @author Daniel Villarreal
 */
class Ciudad {
    private $codigo;
    private $nombre;
    private $codigoDepartamento;

    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->codigo = $datos['codigo'];
                $this->nombre = $datos['nombre'];
                $this->codigoDepartamento = $datos['coddepartamento'];
            } else {
                $cadenaSQL = "select * from ciudad where codigo = '$datos'";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                //print_r($resultado);
                if (count($resultado)) {
                    $this->codigo = $datos;
                    $this->nombre = $resultado[0]['nombre'];
                    $this->codigoDepartamento = $resultado[0]['coddepartamento'];
                }
            }
        }
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getCodigoDepartamento() {
        return $this->codigoDepartamento;
    }
    
    function getDepartamento(){
        return new Departamento($this->codigoDepartamento);
    }

    function setCodigo($codigo): void {
        $this->codigo = $codigo;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    public function __toString() {
        return $this->getNombre();   
    }
    
    function setCodigoDepartamento($codigoDepartamento): void {
        $this->codigoDepartamento = $codigoDepartamento;
    }

    function guardar(){
        $cadenaSQL = "insert into ciudad(codigo, nombre, codDepartamento) values ('{$this->codigo}','{$this->nombre}','{$this->codigoDepartamento}')";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function modificar($codigoAnterio) {
        $cadenaSQL = "update ciudad set codigo='{$this->codigo}', nombre='{$this->nombre}', codDepartamento='{$this->codigoDepartamento}' where codigo= $codigoAnterio";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from ciudad where codigo = '{$this->codigo}'";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro, $orden) {
        if ($filtro != null) $filtro = " where $filtro";
        else $filtro = '';
        if ($orden != null) $orden = " order by $orden";
        else $orden = '';
        $cadenaSQL = "select codigo, nombre, codDepartamento from ciudad $filtro $orden";
        //echo '********** '.$cadenaSQL;
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaInObject($filtro, $orden) {
        $resultado = Ciudad::getLista($filtro, $orden);
        $ciudades = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $ciudad = new Ciudad($resultado[$i]);
                $ciudades[] = $ciudad;
            }
        }
        return $ciudades;
    }
    
    static function getListaInOptions($predeterminado) {
        $lista = "";
        $resultado = Ciudad::getListaInObject(null, "nombre");
        if ($resultado != null) {
            $auxiliar = "";
            for ($i = 0; $i < count($resultado); $i++) {
                $ciudad = $resultado[$i];
                if ($predeterminado == $ciudad->getCodigo()) $auxiliar = "selected";
                $lista .= "<option value='{$ciudad->getCodigo()}' $auxiliar>{$ciudad->getNombre()}</option>";
            }
        }
        return $lista;
    }
}
