<?php

/**
 * Description of Pais
 *
 * @author Daniel Villarreal
 */
class Departamento {

    private $codigo;
    private $nombre;
    private $codigoPais;

    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->codigo = $datos['codigo'];
                $this->nombre = $datos['nombre'];
                $this->codigoPais = $datos['codpais'];
            } else {
                $cadenaSQL = "select * from departamento where codigo = '$datos'";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)) {
                    $this->codigo = $resultado[0]['codigo'];
                    $this->nombre = $resultado[0]['nombre'];
                    $this->codigoPais = $resultado[0]['codpais'];
                }
            }
        }
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getCodigoPais() {
        return $this->codigoPais;
    }
    
    function getPais(){
        return new Pais($this->codigoPais);
    }

    function setCodigo($codigo): void {
        $this->codigo = $codigo;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    function setCodigoPais($codigoPais): void {
        $this->codigoPais = $codigoPais;
    }

    function guardar(){
        $cadenaSQL = "insert into departamento(codigo, nombre, codPais) values ('{$this->codigo}','{$this->nombre}','{$this->codigoPais}')";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function modificar($codigoAnterio) {
        $cadenaSQL = "update departamento set codigo='{$this->codigo}', nombre='{$this->nombre}', codPais='{$this->codigoPais}' where codigo= $codigoAnterio";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from departamento where codigo = '{$this->codigo}'";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro, $orden) {
        if ($filtro != null) $filtro = " where $filtro";
        else $filtro = '';
        if ($orden != null) $orden = " order by $orden";
        else $orden = '';
        $cadenaSQL = "select codigo, nombre, codPais from departamento $filtro $orden";
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaInObject($filtro, $orden) {
        $resultado = Departamento::getLista($filtro, $orden);
        $departamentos = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $departamento = new Departamento($resultado[$i]);
                $departamentos[] = $departamento;
            }
        }
        return $departamentos;
    }
    
    static function getListaInOptions($predeterminado) {
        $lista = "";
        $resultado = Departamento::getListaInObject(null, "nombre");
        if ($resultado != null) {
            $auxiliar = "";
            for ($i = 0; $i < count($resultado); $i++) {
                $departamento = $resultado[$i];
                if ($auxiliar == $departamento['codigo']) $auxiliar = "selected";
                $lista .= "<option value='{$departamento->getCodigo()}' $auxiliar>{$departamento->getNombre()}</option>";
            }
        }
        return $lista;
    }
}
