<?php


class Empresa {

    private $id;
    private $nit;
    private $nombre;
    private $direccion;
    private $telefono;
    private $codciudad;
    private $email;
    private $tipo;
    
    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->id = $datos['id'];
                $this->nit = $datos['nit'];
                $this->nombre = $datos['nombre'];
                $this->direccion = $datos['direccion'];
                $this->telefono = $datos['telefono'];
                $this->codciudad = $datos['codciudad'];
                $this->email = $datos['email'];
                $this->tipo = $datos['tipo'];
            } else {
                $cadenaSQL = "select * from empresa where id = $datos";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)>0) {
                    $this->id = $datos;
                    $this->nit = $resultado[0]['nit'];
                    $this->nombre = $resultado[0]['nombre'];
                    $this->direccion = $resultado[0]['direccion'];
                    $this->telefono = $resultado[0]['telefono'];
                    $this->codciudad = $resultado[0]['codciudad'];
                    $this->email = $resultado[0]['email'];
                    $this->tipo = $resultado[0]['tipo'];
                }

            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getNit() {
        return $this->nit;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCodciudad() {
        return $this->codciudad;
    }
    
    function getCiudad(){
        return new Ciudad($this->codciudad);
    }

    function getEmail() {
        return $this->email;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getPersona(){
        $resulado = Persona::getListaEnObjetos(" identificacion, nombres, apellidos, telefono, email, clave, tipo, idempresa, idsucursal from persona where tipo='E' and idempresa = $this->id");
        if($resulado != null) return $resulado[0];
        else return null;
        
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setNit($nit): void {
        $this->nit = $nit;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion): void {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono): void {
        $this->telefono = $telefono;
    }

    function setCodciudad($codciudad): void {
        $this->codciudad = $codciudad;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setTipo($tipo): void {
        $this->tipo = $tipo;
    }

    public function __toString() {
        return $this->nombre;
    }

    
    function guardar(){
        $cadenaSQL = "insert into empresa (nit, nombre, direccion, telefono, codciudad, email, tipo) "
                . "values('$this->nit','$this->nombre','$this->direccion','$this->telefono',$this->codciudad,'$this->email','$this->tipo')";
        ConectorBD::ejecutarQuery($cadenaSQL);
        $cadenaSQL = "select max(id) as id from empresa";
        $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
        //echo $cadenaSQL;
        if (count($resultado)) $this->id = $resultado[0]['id'];
    }
    
    function modificar(){
        $cadenaSQL = "update empresa set nit='$this->nit', nombre='$this->nombre', direccion='$this->direccion', telefono='$this->telefono', "
                . "codciudad=$this->codciudad, email='$this->email', tipo='$this->tipo' where id = $this->id";
        //echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from empresa where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    static function getLista($filtro){
        $cadenaSQL = "select $filtro";
        //echo $cadenaSQL;
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = Empresa::getLista($filtro);
        $empresas = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $empresas[] = new Empresa($resultado[$i]);
            }
        }
        return $empresas;
    }

    static function getProveedoresEnOpciones($predeterminado){
        $resultado=Empresa::getLista(" * from empresa where tipo ='P' ");
        $retorno='';

        for ($i=0; $i <count($resultado) ; $i++) { 
            $auxiliar='';
            if($resultado[$i]['id']==$predeterminado) $auxiliar='selected';
            $retorno.="<option $auxiliar value='{$resultado[$i]['id']}' >{$resultado[$i]['nombre']}</option>";
        }
        return $retorno;
    }

    

}
