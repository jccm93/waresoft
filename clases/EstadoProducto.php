<?php

class EstadoProducto {

    private $codigo;
    private $nombre;

    function __construct($codigo) {
        switch ($codigo) {
            case '1':
                $this->nombre = 'Disponible';
                break;
            case '2':
                $this->nombre = 'No disponible';
                break;
        }
    }
    
    public function __toString() {
        return $this->getNombre();
    }

    static function getEstadoProductoEnOpciones($codigo){
        $retorno='';
        switch ($codigo) {
            case '1':
                $retorno='<option value=1 selected>Disponible</option><option value=2 >No disponible</option>';
                break;
            case '2':
                $retorno='<option value=1 >Disponible</option><option value=2 selected>No disponible</option>';
                break;
            default: 
                $retorno='<option value=1 selected>Disponible</option><option value=2 >No disponible</option>';
                break;
        }
        return $retorno;
    }
}
