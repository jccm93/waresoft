<?php


class Factura {
    private $id;
    private $fecha;
    private $fechaEntrega;
    private $idSucursal;
    private $estadoFactura;
    private $idVendedor;
    private $identificacionCliente;
    
    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->id = $datos['id'];
                $this->fecha = $datos['fecha'];
                $this->fechaEntrega = $datos['fechaentrega'];
                $this->idSucursal = $datos['idsucursal'];
                $this->estadoFactura = $datos['estadofactura'];
                $this->idVendedor = $datos['idvendedor'];
                $this->identificacionCliente = $datos['$identificacioncliente'];
            }else{
                $cadenaSQL = "select *, fecha::timestamp(0) from factura where id = $datos";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)) {
                    $this->id = $datos;
                    $this->fecha = $resultado[0]['fecha'];
                    $this->fechaEntrega = $resultado[0]['fechaentrega'];
                    $this->idSucursal = $resultado[0]['idsucursal'];
                    $this->estadoFactura = $resultado[0]['estadofactura'];
                    $this->idVendedor = $resultado[0]['idvendedor'];
                    $this->identificacionCliente = $resultado[0]['identificacioncliente'];
                }
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getFechaEntrega() {
        return $this->fechaEntrega;
    }

    function getIdSucursal() {
        return $this->idSucursal;
    }
    
    function getSucursal(){
        return new Sucursal($this->idSucursal);
    }

    function getEstadoFactura() {
        return $this->estadoFactura;
    }

    function getIdVendedor() {
        return $this->idVendedor;
    }

    function getVendedor(){
        return new Persona($this->idVendedor);
    }

    function getIdentificacionCliente() {
        return $this->identificacionCliente;
    }
    
    function getCliente(){
        return new Persona($this->identificacionCliente);
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setFecha($fecha): void {
        $this->fecha = $fecha;
    }

    function setFechaEntrega($fechaEntrega): void {
        $this->fechaEntrega = $fechaEntrega;
    }

    function setIdSucursal($idSucursal): void {
        $this->idSucursal = $idSucursal;
    }

    function setEstadoFactura($estadoFactura): void {
        $this->estadoFactura = $estadoFactura;
    }

    function setIdVendedor($idVendedor): void {
        $this->idVendedor = $idVendedor;
    }

    function setIdentificacionCliente($identificacionCliente): void {
        $this->identificacionCliente = $identificacionCliente;
    }

    function guardar(){
        echo 'entre a guardar';
        $cadenaSQL = "insert into factura(fecha,fechaentrega,idsucursal,estadofactura,idvendedor,identificacioncliente) "
                . "values(now(), ".($this->fechaEntrega==null || $this->fechaEntrega=='' ? ' null ':"'{$this->getFechaEntrega}'" )." ,$this->idSucursal,'$this->estadoFactura',$this->idVendedor,'$this->identificacionCliente') returning id";
        $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
        echo $cadenaSQL;
        $this->id = $resultado[0]['id'];
    }

    function modificar(){
        $cadenaSQL = "update factura set fecha='$this->fecha',fechaentrega='$this->fechaEntrega',idsucursal=$this->idSucursal,estadofactura='$this->estadoFactura'"
                . ",idvendedor=$this->idVendedor,identificacioncliente='$this->identificacionCliente' where id = $this->id ";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    function entregarFactura(){
        $cadenaSQL = "update factura set fechaentrega=now(), estado='E' where id = $this->id ";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    function eliminar(){
        $cadenaSQL = "delete from factura where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro){
        $cadenaSQL = "select $filtro";
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = Factura::getLista($filtro);
        $facturas = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $facturas[] = new Empresa($resultado[$i]);
            }
        }
        return $facturas;
    } 
}
