<?php

class FacturaDetalle {
    private $id;
    private $idFactura;
    private $idProducto;
    private $cantidad;
    private $iva;
    private $valorUnitario;
    
    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->id = $datos['id'];
                $this->idFactura = $datos['idfactura'];
                $this->idProducto = $datos['idproducto'];
                $this->cantidad = $datos['cantidad'];
                $this->iva = $datos['iva'];
                $this->valorUnitario = $datos['valorUnitario'];
            } else {
                $cadenaSQL = "select * from facturaproducto where id = $this->id";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)) {
                    $this->id = $datos;
                    $this->idFactura = $resultado[0]['idfactura'];
                    $this->idProducto = $resultado[0]['idproducto'];
                    $this->cantidad = $resultado[0]['cantidad'];
                    $this->iva = $resultado[0]['iva'];
                    $this->valorUnitario = $resultado[0]['valorUnitario'];
                }
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getIdFactura() {
        return $this->idFactura;
    }

    function getFactura(){
        return new Factura($this->idFactura);
    }

    function getIdProducto() {
        return $this->idProducto;
    }

    function getProducto(){
        return $this->idProducto;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getIva() {
        return $this->iva;
    }

    function getValorUnitario() {
        return $this->valorUnitario;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setIdFactura($idFactura): void {
        $this->idFactura = $idFactura;
    }

    function setIdProducto($idProducto): void {
        $this->idProducto = $idProducto;
    }

    function setCantidad($cantidad): void {
        $this->cantidad = $cantidad;
    }

    function setIva($iva): void {
        $this->iva = $iva;
    }

    function setValorUnitario($valorUnitario): void {
        $this->valorUnitario = $valorUnitario;
    }

    function guardar(){
        $cadenaSQL = "insert into facturaproducto (idfactura,idproducto,cantidad,iva,valorunitario) "
                . "values($this->idFactura,$this->idProducto,$this->cantidad,$this->iva,$this->valorUnitario)";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function modificar(){
        $cadenaSQL = "update facturaproducto set idfactura=$this->idFactura,idproducto=$this->idProducto,cantidad=$this->cantidad,iva=$this->iva,valorunitario=$this->valorUnitario where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from facturaproducto where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro) {
        $cadenaSQL = "select $filtro";
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = FacturaDetalle::getLista($filtro);
        $facturasDetalle = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $facturasDetalle[] = new FacturaDetalle($resultado[$i]);
            }
        }
        return $facturasDetalle;
    }
    

}
