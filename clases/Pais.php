<?php
/**
 * Description of Pais
 *
 * @author Daniel Villarreal
 */
class Pais {

    private $codigo;
    private $nombre;

    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->codigo = $datos['codigo'];
                $this->nombre = $datos['nombre'];
            } else {
                $cadenaSQL = "select * from pais where codigo = '$datos'";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)) {
                    $this->codigo = $resultado[0]['codigo'];
                    $this->nombre = $resultado[0]['nombre'];
                }
            }
        }
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setCodigo($codigo): void {
        $this->codigo = $codigo;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    public function __toString() {
        return $this->getNombre();
    }

    
    function guardar(){
        $cadenaSQL = "insert into pais (codigo, nombre) values ('{$this->codigo}','{$this->nombre}')";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function modificar($codigoAnterio) {
        $cadenaSQL = "update pais set codigo='{$this->codigo}', nombre='{$this->nombre}' where codigo= '$codigoAnterio'";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from pais where codigo = '{$this->codigo}'";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro, $orden) {
        if ($filtro != null) $filtro = " where $filtro";
        else $filtro = '';
        if ($orden != null) $orden = " order by $orden";
        else $orden = '';
        $cadenaSQL = "select codigo, nombre from pais $filtro $orden";
        //echo $cadenaSQL;
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaInObject($filtro, $orden) {
        $resultado = Pais::getLista($filtro, $orden);
        $paises = null;
       // if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $pais = new Pais($resultado[$i]);
                $paises[] = $pais;
            }
        //}
        return $paises;
    }
    
    static function getListaInOptions($predeterminado) {
        $lista = "";
        echo '**********************************'.$predeterminado;
        $resultado = Pais::getListaInObject(null, "nombre");
        //print_r($resultado);
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $auxiliar = "";
                $pais = $resultado[$i];
                if ($predeterminado == $pais->getCodigo()) $auxiliar = "selected";
                $lista .= "<option value='{$pais->getCodigo()}' $auxiliar>{$pais->getNombre()}</option>";
            }
        }
        return $lista;
    }
}
