<?php

class Persona {
    private $identificacion;
    private $nombres;
    private $apellidos;
    private $telefono;
    private $email;
    private $clave;
    private $tipo;
    private $idempresa;
    private $idsucursal;

    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->identificacion = $datos['identificacion'];
                $this->nombres= $datos['nombres'];
                $this->apellidos= $datos['apellidos'];
                $this->telefono = $datos['telefono'];
                $this->email= $datos['email'];
                $this->clave= $datos['clave'];
                $this->tipo= $datos['tipo'];
                $this->idempresa= $datos['idempresa'];
                $this->idsucursal= $datos['idsucursal'];
            } else {
                $cadenaSQL = "select * from persona where identificacion = '$datos'";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if(count($resultado)){
                    $this->identificacion = $datos;
                    $this->nombres= $resultado[0]['nombres'];
                    $this->apellidos= $resultado[0]['apellidos'];
                    $this->telefono = $resultado[0]['telefono'];
                    $this->email= $resultado[0]['email'];
                    $this->clave= $resultado[0]['clave'];
                    $this->tipo= $resultado[0]['tipo'];
                    $this->idempresa= $resultado[0]['idempresa'];
                    $this->idsucursal= $resultado[0]['idsucursal'];
                }
            }
        }
    }
    
    function getIdentificacion() {
        return $this->identificacion;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function getClave() {
        return $this->clave;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getIdempresa() {
        return $this->idempresa;
    }

    function getEmpresa(){
        return new Empresa($this->idempresa);
    }

    function getIdSucursal(){
        return $this->idsucursal;
    }

    function setIdSucursal($idSucursal): void {
        $this->idsucursal=$idSucursal;
    }

    function setIdentificacion($identificacion): void {
        $this->identificacion = $identificacion;
    }

    function setNombres($nombres): void {
        $this->nombres = $nombres;
    }

    function setApellidos($apellidos): void {
        $this->apellidos = $apellidos;
    }

    function setTelefono($telefono): void {
        $this->telefono = $telefono;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setClave($clave): void {
        if(strlen($clave)<32) $this->clave = "md5('$clave')";
        else $this->clave = '\''.$clave.'\'';
    }

    function setTipo($tipo): void {
        $this->tipo = $tipo;
    }

    function setIdempresa($idempresa): void {
        $this->idempresa = $idempresa;
    }

    public function __toString() {
        return $this->nombres.' '.$this->apellidos;
    }

    public function getEmpresaEnObjeto(){
        if($this->idempresa!=null || $this->idempresa=='') return new Empresa(null);
        else return new Empresa($this->idempresa);
    }

    public function getSucursalEnObjeto(){
        if($this->idsucursal || $this->idsucursal=='') return new Sucursal(null);
        else return new Sucursal($this->idsucursal);
    }
    
    function guardar(){
        if ($this->idsucursal == null || $this->idsucursal=='') $this->idsucursal = "null";
        $cadenaSQL = "insert into persona(identificacion, nombres, apellidos, telefono, email, clave, tipo, idempresa, idsucursal) "
                . "values('$this->identificacion','$this->nombres','$this->apellidos','$this->telefono','$this->email',$this->clave,'$this->tipo',$this->idempresa, $this->idsucursal)";
        //echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function modificar($identificacionAnterior){
        if ($this->idsucursal == null || $this->idsucursal=='') $this->idsucursal = "null";
        $cadenaSQL = "update persona set identificacion='$this->identificacion', nombres='$this->nombres', apellidos='$this->apellidos', "
                . "telefono='$this->telefono', email='$this->email', clave=$this->clave, tipo='$this->tipo', idempresa='$this->idempresa', idsucursal=$this->idsucursal where identificacion = '$identificacionAnterior'";
        //echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    function modificarFactura(){
        $cadenaSQL = "update persona set nombres='$this->nombres', apellidos='$this->apellidos', "
                . "telefono='$this->telefono' where identificacion = '$this->identificacion'";
        //echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    function eliminar(){
        $cadenaSQL = "delete from persona where identificacion = '$this->identificacion'";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro){
        $cadenaSQL = "select $filtro";
        //echo $cadenaSQL;
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = Persona::getLista($filtro);
        $personas = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $personas[] = new Persona($resultado[$i]);
            }
        }
        return $personas;
    }
    
    static function validar($identificacion, $clave) {
        $persona = Persona::getListaEnObjetos("* from persona where identificacion = '$identificacion' and clave = md5('$clave')", null);
        //print_r($persona->getApellido());
        if (is_array($persona)) return $persona[0];
        else return null;
        }

}
