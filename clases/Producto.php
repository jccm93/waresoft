<?php

class Producto {
    private $id;
    private $nombre;
    private $stock;
    private $stockMinimo;
    private $stockMaximo;
    private $stockInicial;
    private $valorUnitario;
    private $iva;
    private $idEmpresa;
    private $disponible;
    private $idProveedor;
    
    function __construct($datos) {
        if($datos != null){
            if (is_array($datos)) {
                $this->id = $datos['id'];
                $this->nombre = $datos['nombre'];
                $this->stock = $datos['stock'];
                $this->stockMinimo = $datos['stockminimo'];
                $this->stockMaximo = $datos['stockmaximo'];
                $this->valorUnitario = $datos['valorunitario'];
                $this->stockInicial = $datos['stockinicial'];
                $this->iva = $datos['iva'];
                $this->idEmpresa = $datos['idempresa'];
                $this->disponible = $datos['disponible'];
                $this->idProveedor = $datos['idproveedor'];
            } else {
                $cadenaSQL = "select * from producto where id = $datos";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if(count($resultado)>0){
                    $this->id = $datos;
                    $this->nombre = $resultado[0]['nombre'];
                    $this->stock = $resultado[0]['stock'];
                    $this->stockMinimo = $resultado[0]['stockminimo'];
                    $this->stockMaximo = $resultado[0]['stockmaximo'];
                    $this->valorUnitario = $resultado[0]['valorunitario'];
                    $this->stockInicial = $resultado[0]['stockinicial'];
                    $this->iva = $resultado[0]['iva'];
                    $this->idEmpresa = $resultado[0]['idempresa'];
                    $this->disponible = $resultado[0]['disponible'];
                    $this->idProveedor = $resultado[0]['idproveedor'];
                }
            }
        }
        
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getStock() {
        return $this->stock;
    }

    function getStockMinimo() {
        return $this->stockMinimo;
    }

    function getStockMaximo() {
        return $this->stockMaximo;
    }

    function getStockInicial(){
        return $this->stockInicial;
    }

    function setStockInicial($stockInicial){
        $this->stockInicial=$stockInicial;
    }

    function getValorUnitario() {
        return $this->valorUnitario;
    }

    function getIva() {
        return $this->iva;
    }

    function getIdEmpresa() {
        return $this->idEmpresa;
    }

    function getEmpresa(){
        return new Empresa($this->idEmpresa);
    }
    
    function getDisponible() {
        return $this->disponible;
    }

    function getIdProveedor(){
        return $this->idProveedor;
    }

    function setIdProveedor($idProveedor){
        $this->idProveedor=$idProveedor;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    function setStock($stock): void {
        $this->stock = $stock;
    }

    function setStockMinimo($stockMinimo): void {
        $this->stockMinimo = $stockMinimo;
    }

    function setStockMaximo($stockMaximo): void {
        $this->stockMaximo = $stockMaximo;
    }

    function setValorUnitario($valorUnitario): void {
        $this->valorUnitario = $valorUnitario;
    }

    function setIva($iva): void {
        $this->iva = $iva;
    }

    function setIdEmpresa($idEmpresa): void {
        $this->idEmpresa = $idEmpresa;
    }

    public function __toString() {
        return $this->nombre; 
    }
    
    function setDisponible($disponible): void {
        $this->disponible = $disponible;
    }

    function guardar(){
        $cadenaSQL = "insert into producto (nombre, stock, stockminimo, stockmaximo, valorunitario, iva, idempresa, disponible, stockinicial, idproveedor) "
                . "values('$this->nombre',$this->stock,$this->stockMinimo,$this->stockMaximo,$this->valorUnitario,$this->iva, $this->idEmpresa, '$this->disponible', '$this->stock', $this->idProveedor)";

        //echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    function modificar(){
        $cadenaSQL = "update producto set nombre='$this->nombre', stock=$this->stock, stockminimo=$this->stockMinimo, stockmaximo=$this->stockMaximo, "
                . "valorunitario=$this->valorUnitario, iva=$this->iva, idempresa=$this->idEmpresa, disponible='$this->disponible', idproveedor=$this->idProveedor, stockinicial={$this->stockInicial} where id = $this->id";
        echo $cadenaSQL;
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from producto where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getLista($filtro) {
        $cadenaSQL = "select $filtro";

        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = Producto::getLista($filtro);
        $productos = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $productos[] = new Producto($resultado[$i]);
            }
        }
        return $productos;
    }
}
