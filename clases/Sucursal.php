<?php


class Sucursal {
    private $id;
    private $nombre;
    private $direccion;
    private $telefono;
    private $codciudad;
    private $email;
    private $idEmpresa;
    
    function __construct($datos) {
        if ($datos != null) {
            if (is_array($datos)) {
                $this->id = $datos['id'];
                $this->nombre = $datos['nombre'];
                $this->direccion = $datos['direccion'];
                $this->telefono = $datos['telefono'];
                $this->codciudad = $datos['codciudad'];
                $this->email = $datos['email'];
                $this->idEmpresa = $datos['idempresa'];
            } else {
                $cadenaSQL = "select * from sucursal where id = $datos";
                $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
                if (count($resultado)) {
                    $this->id = $datos;
                    $this->nombre = $resultado[0]['nombre'];
                    $this->direccion = $resultado[0]['direccion'];
                    $this->telefono = $resultado[0]['telefono'];
                    $this->codciudad = $resultado[0]['codciudad'];
                    $this->email = $resultado[0]['email'];
                    $this->idEmpresa = $resultado[0]['idempresa'];
                }

            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCodciudad() {
        return $this->codciudad;
    }
    
    function getCiudad(){
        return new Ciudad($this->codciudad);
    }

    function getEmail() {
        return $this->email;
    }

    function getIdEmpresa() {
        return $this->idEmpresa;
    }

    function getEmpresa(){
        return new Empresa($this->idEmpresa);
    }

    function getPersona(){
        $resulado = Persona::getListaEnObjetos("identificacion, nombres, apellidos, telefono, email, clave, tipo, idempresa, idsucursal from persona where idsucursal = $this->id");
        if($resulado != null)return $resulado[0];
        else return null;
        
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion): void {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono): void {
        $this->telefono = $telefono;
    }

    function setCodciudad($codciudad): void {
        $this->codciudad = $codciudad;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setIdEmpresa($idEmpresa): void {
        $this->idEmpresa = $idEmpresa;
    }

    public function __toString() {
        return $this->nombre;
    }
    
    function guardar(){
        $cadenaSQL = "insert into sucursal (nombre, direccion, telefono, codciudad, email, idempresa) "
                . "values('$this->nombre','$this->direccion','$this->telefono','$this->codciudad','$this->email','$this->idEmpresa')";
        $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
        $cadenaSQL = "select max(id) as id from sucursal";
        $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
        //echo $cadenaSQL;
        if (count($resultado)) $this->id = $resultado[0]['id'];
    }
    
    function modificar(){
        $cadenaSQL = "update sucursal set nombre='$this->nombre', direccion='$this->direccion', telefono='$this->telefono', "
                . "codciudad='$this->codciudad', email='$this->email' where id = $this->id";
        $resultado = ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    function eliminar(){
        $cadenaSQL = "delete from sucursal where id = $this->id";
        ConectorBD::ejecutarQuery($cadenaSQL);
    }

    static function getLista($filtro){
        $cadenaSQL = "select $filtro";
        //echo $cadenaSQL;
        return ConectorBD::ejecutarQuery($cadenaSQL);
    }
    
    static function getListaEnObjetos($filtro){
        $resultado = Sucursal::getLista($filtro);
        $sucursales = null;
        if ($resultado != null) {
            for ($i = 0; $i < count($resultado); $i++) {
                $sucursales[] = new Sucursal($resultado[$i]);
            }
        }
        return $sucursales;
    } 

    static function getListaEnOpciones( $idempresa, $seleccionado){
        $retorno='';
        $resultado=Sucursal::getLista(' * from sucursal where idempresa = '.$idempresa);
        
        foreach ($resultado as $key => $value) {
            $auxiliar='';
            if($value['id']==$seleccionado) $auxiliar='selected';
            $retorno.="<option $auxiliar value='{$value['id']}'>{$value['nombre']}</option>";
        }
        return $retorno;
    }

}
