<?php

class TipoEmpresa {
    private $codigo;
    
    function __construct($codigo) {
        $this->codigo = $codigo;
    }
    
    function getCodigo() {
        return $this->codigo;
    }

    function getNombre(){
        $tipo = "";
        switch ($this->codigo) {
            case 'V':$tipo = 'Vendedor';break;
            case 'P':$tipo = 'Proveedor';break;
        }
    }
    
    public function __toString() {
        return $this->getNombre();
    }
    
    static function getListaEnObjetos() {
        $tipos[] = new TipoPersona('V'); 
        $tipos[] = new TipoPersona('P'); 
        return $tipos;
    }



}
