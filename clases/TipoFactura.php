<?php

class TipoFactura {

    function __construct($codigo) {
        $this->codigo = $codigo;
    }
    
    function getCodigo() {
        return $this->codigo;
    }

    function getNombre(){
        $tipo = "";
        switch ($this->codigo) {
            case 'E':$tipo = 'Entregado';break;
            case 'P':$tipo = 'Pendiente';break;
        }
    }
    
    public function __toString() {
        return $this->getNombre();
    }
    
    static function getListaEnObjetos() {
        $tipos[] = new TipoPersona('E'); 
        $tipos[] = new TipoPersona('P'); 
        return $tipos;
    }
}
