<?php

class TipoPersona {
    private $codigo;
    
    function __construct($codigo) {
        $this->codigo = $codigo;
    }
    
    function getCodigo() {
        return $this->codigo;
    }

    function getNombre(){
        $tipo = "";
        switch ($this->codigo) {
            case 'A':$tipo = "Administrador del sistema";break;
            case 'E':$tipo = "Administrador de la empresa";break;
            case 'S':$tipo = "Administrador de la sucursal";break;
            case 'V':$tipo = "Vendedor";break;
            case 'B':$tipo = "Bodega";break;
            case 'U':$tipo = "Usuario";break;
        }
        return $tipo;   
    }
    
    public function __toString() {
        return $this->getNombre();
    }

    static function getListaEnObjetos() {
        $tipos[] = new TipoPersona('A'); 
        $tipos[] = new TipoPersona('E'); 
        $tipos[] = new TipoPersona('S'); 
        $tipos[] = new TipoPersona('V'); 
        $tipos[] = new TipoPersona('B'); 
        $tipos[] = new TipoPersona('U'); 
        return $tipos;
    }

    static function getBodegaVendedorEnOpciones($codigo){
        $retorno='';

        switch ($codigo) {
            case 'B':
                $retorno='<option value=T >Todos</option><option value=B selected>Bodega</option><option value=V >Vendedor</option>';
                break;
            case 'V':
                $retorno='<option value=T >Todos</option><option value=B >Bodega</option><option value=V selected>Vendedor</option>';
                break;
            default:
                $retorno='<option value=T selected>Todos</option><option value=B >Bodega</option><option value=V >Vendedor</option>';
                break;
        }
        return $retorno;
    }

    static function getBodegaOVendedorEnOpciones($codigo){
        $retorno='';

        switch ($codigo) {
            case 'B':
                $retorno='<option value=B selected>Bodega</option><option value=V >Vendedor</option>';
                break;
            case 'V':
                $retorno='<option value=B >Bodega</option><option value=V selected>Vendedor</option>';
                break;
            default:
                $retorno='<option value=B selected>Bodega</option><option value=V >Vendedor</option>';
                break;
        }
        return $retorno;
    }

}
