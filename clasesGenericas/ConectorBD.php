<?php

class ConectorBD {
    private $servidor;
    private $puerto;
    private $usuario;
    private $clave;
    private $baseDatos;

    public $conexion;

    function __construct() {
        $archivo = dirname(__FILE__) . '/../configuracion.ini'; //Busca el archivo
        if (file_exists($archivo)) {
            $datos = parse_ini_file($archivo);
            $this -> servidor = $datos['servidor'];
            $this -> puerto = $datos['puerto'];
            $this -> usuario = $datos['usuario'];
            $this -> clave = $datos['clave'];
            $this -> baseDatos = $datos['baseDatos'];
        }else echo 'No se encontró el archivo de configuración.';
    }

    function conectar(){
        try {
            //$opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            //$this -> conexion = new PDO("mysql:host={$this->servidor}; port={$this->puerto}; dbname={$this->baseDatos}", $this->usuario, $this->clave, $opciones);
            $this -> conexion = new PDO("pgsql:host={$this->servidor}; port={$this->puerto}; dbname={$this->baseDatos}", $this->usuario, $this->clave);
            //echo 'conectado';
        } catch (Exception $exc) {
            //echo $exc ->getTraceAsString();
            echo "<p>Error al conectarse a la base de datos. {$exc ->getMessage()}";
        }
    }

    function desconectar(){
        $this -> conexion = null;
    }
    
    static function ejecutarQuery($cadenaSQL){
        $conector = new ConectorBD();
        $conector->conectar();
        $sentencia = $conector->conexion->prepare($cadenaSQL);
        if(!$sentencia->execute()) echo "Error al ejecutar $cadenaSQL";
        else $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $conector->desconectar();
        return $resultado;
    }
}
?>