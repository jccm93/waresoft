<?php
    include_once './clases/Empresa.php';
    include_once './clases/Sucursal.php';
    include_once './clases/TipoPersona.php';

    

    if($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='E') $idEmpresa=$USUARIOINGRESADO->getIdEmpresa(); 
    else $idEmpresa=$_REQUEST['idempresa'];
    $empresa=new Empresa($idEmpresa);

    $lista='';
    $filtro=' where persona.idempresa = '.$idEmpresa.' and persona.tipo <> \'E\' and persona.tipo <> \'S\' ';

    $cargo='';
    $identificacion='';
    $sucursalBusqueda='';

    if(isset($_REQUEST['cargo']) || isset($_REQUEST['identificacion']) || isset($_REQUEST['sucursal'])){
        if($_REQUEST['cargo'] && $_REQUEST['cargo']!='T'){
            $filtro.=' and tipo = \''.$_REQUEST['cargo'].'\' ';
            $cargo=$_REQUEST['cargo'];
        }
        if($_REQUEST['identificacion']){
            $filtro.=' and identificacion like \'%'.$_REQUEST['identificacion'].'%\' ';
            $identificacion=$_REQUEST['identificacion'];
        }
        if($_REQUEST['sucursal'] && $_REQUEST['sucursal']!='T'){
            $filtro.=' and persona.idsucursal = '.$_REQUEST['sucursal'].' ';
            $sucursalBusqueda=$_REQUEST['sucursal'];
        }
    }

    $resultado=Persona::getLista(' identificacion, nombres, apellidos, tipo, nombre, persona.telefono, persona.email from persona join sucursal on sucursal.id=persona.idsucursal '.$filtro.' order by identificacion');
    
    foreach ($resultado as $key => $datos) {
        $lista.="<tr>";
        $lista.="<td>{$datos['identificacion']}</td>";
        $lista.="<td>{$datos['nombres']} {$datos['apellidos']}</td>";
        $lista.="<td>".new TipoPersona($datos['tipo'])."</td>";
        $lista.="<td>{$datos['nombre']}</td>";
        $lista.="<td>{$datos['telefono']}</td>";
        $lista.="<td>{$datos['email']}</td>";
        $lista.="<td><a href='principal.php?contenido=colaboradoresFormulario.php". (($_SESSION['usuario']->getTipo()=='A') ? 'idsucursal='.$_REQUEST['idempresa'].'&' : '' ) ."&id={$datos['identificacion']}' ><i class='fas fa-edit'></i></a><a href='principal.php?contenido=colaboradoresFormulario.php&accion=Eliminar&id={$datos['identificacion']}' ><i class='fas fa-trash'></i></a></td>";
        $lista.="</tr>";
    }

?>
<div class="col-12">
    <h3>LISTA DE COLABORADORES EMPRESA <?= strtoupper($empresa) ?></h3>
    <br>
    <div class="row">
        <form name="formulario" method="POST" action="principal.php?contenido=colaboradores.php">
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Identificacion:</label>
                <div class="col-sm-3 mb-2">
                    <input type="text" class="form-control" name="identificacion" value="<?= $identificacion ?>">
                </div>

                <label for="" class="col-sm-1 col-form-label">Cargo:</label>
                <div class="col-sm-3 mb-2">
                    <select class="form-control" name="cargo">
                        <?= TipoPersona::getBodegaVendedorEnOpciones($cargo) ?>
                    </select>
                </div>
                
                <label for="" class="col-sm-1 col-form-label">Sucursal:</label>
                <div class="col-sm-2 mb-2">
                    <select class="form-control" name="sucursal">
                        <option value="T">Todas</option>
                        <?= Sucursal::getListaEnOpciones( (($USUARIOINGRESADO->getTipo()=='A') ? $_REQUEST['idempresa'] : $USUARIOINGRESADO->getIdEmpresa()), $sucursalBusqueda) ?>
                    </select>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary mb-2">Buscar</button>
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Identificacion</th>
                        <th>Nombres</th>
                        <th>Cargo</th>
                        <th>Sucursal</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Gestion <a href="principal.php?contenido=colaboradoresFormulario.php<?= (($_SESSION['usuario']->getTipo()=='A') ? '&idempresa='.$_REQUEST['idempresa'] : '' ) ?>"><i class="fas fa-plus"></i></a> </th>
                    </tr>
                </thead>
                <tbody>
                    <?= $lista ?>
                </tbody>
            </table>
        </div>
    </div>
</div>