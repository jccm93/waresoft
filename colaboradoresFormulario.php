<?php
    include_once './clases/Empresa.php';
    include_once './clases/Empresa.php';
    include_once './clases/Sucursal.php';
    include_once './clases/TipoPersona.php';

    if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Adicionar'){
        if($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='E'){
           $_REQUEST['idempresa']=$USUARIOINGRESADO->getIdEmpresa(); 
        }
        $persona=new  Persona($_REQUEST);
        $persona->setClave($_REQUEST['clave']);
        $persona->guardar();
        //print_r($persona);
        //print_r($_REQUEST);
        header("Location: principal.php?contenido=colaboradores.php");
    }else if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Modificar'){
        if($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='E'){
           $_REQUEST['idempresa']=$USUARIOINGRESADO->getIdEmpresa(); 
        }
        $persona=new  Persona($_REQUEST);
        $persona->setClave($_REQUEST['clave']);
        $persona->modificar($_REQUEST['identificacionanterior']);
        header("Location: principal.php?contenido=colaboradores.php");
    }else if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Eliminar'){;
        $persona=new  Persona(null);
        $persona->setIdentificacion($_REQUEST['id']);
        //$persona->eliminar();
        //header("Location: principal.php?contenido=colaboradores.php");
    }

    $accion='Adicionar';
    if(isset($_REQUEST['id'])){
        $persona=new  Persona($_REQUEST['id']);
        $accion='Modificar';
    }else $persona=new  Persona(null);

?>
<div class="col-12">
    <h2>ADICIONAR COLABORADOR</h2><br>
    <form name="formulario" method="POST" autocomplete="off" action="principal.php?contenido=colaboradoresFormulario.php">
        <div class="form-group row">
            <label for="" class="col-sm-1 col-form-label">Identificacion:</label>
            <div class="col-sm-3 mb-2">
                <input required type="number" class="form-control"   name="identificacion" value="<?= $persona->getIdentificacion() ?>">
            </div>
            
            <label for="" class="col-sm-1 col-form-label">Nombres:</label>
            <div class="col-sm-3 mb-2">
                <input required type="text" class="form-control" name="nombres" value="<?= $persona->getNombres() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Apellidos:</label>
            <div class="col-sm-3 mb-2">
                <input required type="text" class="form-control"   name="apellidos" value="<?= $persona->getApellidos() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Telefono:</label>
            <div class="col-sm-3 mb-3">
                <input type="number" class="form-control"   name="telefono" value="<?= $persona->getTelefono() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Email:</label>
            <div class="col-sm-3 mb-3">
                <input type="email" class="form-control"   name="email" value="<?= $persona->getEmail() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Clave:</label>
            <div class="col-sm-3 mb-3">
                <input required type="password" class="form-control"   name="clave" value="<?= $persona->getClave() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Cargo:</label>
            <div class="col-sm-3 mb-2">
                <select required class="form-control" name="tipo">
                    <?= TipoPersona::getBodegaOVendedorEnOpciones($persona->getTipo()) ?>
                </select>
            </div>

            <label for="" class="col-sm-1 col-form-label">Sucursal:</label>
            <div class="col-sm-3 mb-2">
                <select class="form-control" name="idsucursal" required>
                    <?= Sucursal::getListaEnOpciones( (($USUARIOINGRESADO->getTipo()=='A') ? $_REQUEST['idempresa'] : $USUARIOINGRESADO->getIdEmpresa()), $persona->getIdSucursal()) ?>
                </select>
            </div>

        </div>
            
        <div class="form-group text-center">
            <input type="hidden" name="identificacionanterior" value="<?= $persona->getIdentificacion() ?>">
            <button type="submit" class="btn btn-primary mb-2" name="accion" value="<?= $accion ?>"><?= $accion ?></button>
        </div>
        
    </form>
</div>