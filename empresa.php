<?php
require_once 'clasesGenericas/ConectorBD.php';
require_once 'clases/Empresa.php';
require_once 'clases/TipoEmpresa.php';
require_once 'clases/Persona.php';
require_once 'clases/Sucursal.php';

$titulo = "DE EMPRESAS";
$filtro = "";
$resultado = "";
$nitTabla = "";
$ocultar = "";
#Ingreso del usuario Administrador de la empresa
if ($USUARIOINGRESADO->getTipo() == 'E') {
    $titulo = "DE SUCURSALES";
    $filtro = "";
    $persona = $USUARIOINGRESADO;
    $ocultar = "style='display:none'";
    $resultado = Sucursal::getListaEnObjetos("id, nombre, direccion,telefono,codciudad,email,idempresa from sucursal where idempresa = {$persona->getIdempresa()}");
#Fin ingreso el usuario Administrador de la sucursal 
#Ingreso del usuario Administrador de la sucursal
} else if ($USUARIOINGRESADO->getTipo() == 'S') {
    $titulo = "DE PROVEEDORES";
    $filtro = "";
    $persona = $USUARIOINGRESADO;
    $resultado = Empresa::getListaEnObjetos("id, nit, nombre, direccion, telefono, codciudad, email, tipo from empresa where tipo = 'P'");
#Fin ingreso el usuario Administrador de la empresa
} else {
    $resultado = Empresa::getListaEnObjetos("id, nit, nombre, direccion, telefono, codciudad, email, tipo from empresa");
    //$nitTabla = "$lista .= '<td>{$resultado[$i]->getNit()}</td>'";
}

$lista = "";

//print_r($resultado);
if ($resultado != null) {
    for ($i = 0; $i < count($resultado); $i++) {
        //$tipoEmpresa = new TipoEmpresa($resultado[$i]['tipo']);
        $lista .= "<tr>";
        //$nitTabla;
        if ($USUARIOINGRESADO->getTipo() != 'E')
        $lista .= "<td>{$resultado[$i]->getNit()}</td>";
        $lista .= "<td>{$resultado[$i]->getNombre()}</td>";
        $lista .= "<td>{$resultado[$i]->getCodciudad()}</td>";
        $lista .= "<td>{$resultado[$i]->getDireccion()}</td>";
        $lista .= "<td>{$resultado[$i]->getTelefono()}</td>";
        $lista .= "<td>{$resultado[$i]->getEmail()}</td>";
        $lista .= "<td>{$resultado[$i]->getPersona()->getNombres()} {$resultado[$i]->getPersona()->getApellidos()}</td>";
        $lista .= "<td>{$resultado[$i]->getPersona()->getTelefono()}</td>";
        $lista .= "<td>";
        $lista .= "<a href='principal.php?contenido=empresaFormulario.php&accion=Modificar&id={$resultado[$i]->getId()}&idPersona={$resultado[$i]->getPersona()->getIdentificacion()}'><i class='fas fa-edit'></i></a>";
        $lista .= "<i class='fa fa-trash' title='Eliminar' onclick='eliminar({$resultado[$i]->getId()},{$resultado[$i]->getPersona()->getIdentificacion()})'></i></a>";
        $lista .= "</td>";
        $lista .= "</tr>";
    }
}
?>

<div class="col-12">
    <h3>LISTA <?= $titulo ?></h3>
    <br>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th<?= $ocultar ?>>NIT</th>
                        <th>Nombre</th>
                        <th>Ciudad</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Responsable</th>
                        <th>Tel.Responsable</th>
                        <th>Gestion <a href="principal.php?contenido=empresaFormulario.php"><i class="fas fa-plus"></i></a> </th>
                    </tr>
                </thead>
                <tbody>
                    <?= $lista ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    function eliminar(id, idPersona) {
        var respuesta = confirm("¿Realmente desea eliminar este registro?");
        if (respuesta) {
            window.location = "principal.php?contenido=empresaActualizar.php&accion=Eliminar&id=" + id + "&idPersona=" + idPersona;
        }
    }
</script>