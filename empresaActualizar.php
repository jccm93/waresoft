<?php
require_once 'clasesGenericas/ConectorBD.php';
require_once 'clases/Empresa.php';
require_once 'clases/Persona.php';
require_once 'clases/Sucursal.php';

if ($USUARIOINGRESADO->getTipo() == 'E') {
    $sucursal = new Sucursal(null);
    $persona = new Persona(null);
    $sucursal->setNombre($_REQUEST['nombre']);
    $sucursal->setDireccion($_REQUEST['direccion']);
    $sucursal->setTelefono($_REQUEST['telefono']);
    $sucursal->setCodciudad($_REQUEST['codciudad']);
    $sucursal->setEmail($_REQUEST['email']);
    $sucursal->setIdEmpresa($USUARIOINGRESADO->getIdempresa());

    $persona->setIdentificacion($_REQUEST['identificacion']);
    $persona->setNombres($_REQUEST['nombres']);
    $persona->setApellidos($_REQUEST['apellidos']);
    $persona->setTelefono($_REQUEST['telefonoPersona']);
    $persona->setEmail($_REQUEST['emailPersona']);
    $persona->setClave($_REQUEST['clave']);
    $persona->setIdempresa($USUARIOINGRESADO->getIdempresa());
    $persona->setTipo('S');

    switch ($_REQUEST['accion']) {
        case 'Adicionar':
            $sucursal->guardar();
            $persona->setIdSucursal($sucursal->getId());
            $persona->guardar();
            break;
        case 'Modificar':
            $sucursal->setId($_REQUEST['id']);
            $sucursal->modificar();
            $persona->setIdSucursal($_REQUEST['id']);
            $persona->modificar($_REQUEST['idPersona']);
            break;
    }
} else {
    $empresa = new Empresa(null);
    $persona = new Persona(null);
    if ($_REQUEST['accion'] == 'Eliminar') {
        $persona = new Persona($_REQUEST['idPersona']);
        $persona->eliminar();
        $empresa->setId($_REQUEST['id']);
        $empresa->eliminar();
    } else {

        $empresa->setNit($_REQUEST['nit']);
        $empresa->setNombre($_REQUEST['nombre']);
        $empresa->setDireccion($_REQUEST['direccion']);
        $empresa->setTelefono($_REQUEST['telefono']);
        $empresa->setCodciudad($_REQUEST['codciudad']);
        $empresa->setEmail($_REQUEST['email']);
        if ($USUARIOINGRESADO->getTipo() == 'S') $empresa->setTipo('P');
        else $empresa->setTipo($_REQUEST['tipo']);
        $persona->setIdentificacion($_REQUEST['identificacion']);
        $persona->setNombres($_REQUEST['nombres']);
        $persona->setApellidos($_REQUEST['apellidos']);
        $persona->setTelefono($_REQUEST['telefonoPersona']);
        $persona->setEmail($_REQUEST['emailPersona']);
        $persona->setClave($_REQUEST['clave']);
        $persona->setIdSucursal(null);
        $persona->setTipo('E');
    }
    switch ($_REQUEST['accion']) {
        case 'Adicionar':
            $empresa->guardar();
            $persona->setIdempresa($empresa->getId());
            $persona->guardar();
            break;
        case 'Modificar':
            $empresa->setId($_REQUEST['id']);
            $empresa->modificar();
            $persona->setIdempresa($_REQUEST['id']);
            $persona->modificar($_REQUEST['idPersona']);
            break;
    }
}
header("location: principal.php?contenido=empresa.php");
        