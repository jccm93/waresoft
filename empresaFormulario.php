<?php
require_once 'clases/Pais.php';
require_once 'clases/Departamento.php';
require_once 'clases/Ciudad.php';
require_once 'clases/Empresa.php';
require_once 'clases/Persona.php';
require_once 'clases/Sucursal.php';
$titulo = "";
$ocultar = "";
$ocultar1 = "";
$accion = 'Adicionar';
if ($USUARIOINGRESADO->getTipo() == 'E') {
    $titulo = "SUCURSAL";
    $ocultar = " style='display:none'";
    $empresa = new Sucursal(null);
    $persona = new Persona(null);
    $departamentoPrincipal = new Departamento(null);
    $ciudadPrincipal = new Ciudad(null);
    $pais = new Pais(null);
    if (isset($_REQUEST['id'])) {
        $accion = "Modificar";
        $empresa = New Sucursal($_REQUEST['id']);
        $persona = $empresa->getPersona();
        $ciudadPrincipal = $empresa->getCiudad();
        $departamentoPrincipal = $ciudadPrincipal->getDepartamento();
        $pais = $departamentoPrincipal->getPais();
    }
} else {
    $titulo = "EMPRESA";
    $empresa = new Empresa(null);
    $persona = new Persona(null);
    $departamentoPrincipal = new Departamento(null);
    $ciudadPrincipal = new Ciudad(null);
    $pais = new Pais(null);
    if ($USUARIOINGRESADO->getTipo() == 'S') {
        $ocultar1 = " style='display:none'";
        $titulo="PROVEEDOR";
    }
    $auxiliar = "<option value='V' selected>Vendedor</option><option value='P'>Proveedor</option>";
    if (isset($_REQUEST['id'])) {

        $accion = "Modificar";
        $empresa = New Empresa($_REQUEST['id']);
        $persona = $empresa->getPersona();
        $ciudadPrincipal = $empresa->getCiudad();
        $departamentoPrincipal = $ciudadPrincipal->getDepartamento();
        $pais = $departamentoPrincipal->getPais();

        if ($empresa->getTipo() == 'V')
            $auxiliar = "<option value='V' selected>Vendedor</option><option value='P'>Proveedor</option>";
        else
            $auxiliar = "<option value='V'>Vendedor</option><option value='P' selected>Proveedor</option>";
    }
}
?>
<div class="col-12">
    <h2>ADICIONAR <?= $titulo ?></h2><br>
    <form name="formulario" method="POST" action="principal.php?contenido=empresaActualizar.php">
        <div class="form-group row">
            <label for="" class="col-sm-1 col-form-label"<?= $ocultar ?>>NIT:</label>
            <div class="col-sm-3 mb-2" <?= $ocultar ?>>
                <?php if ($USUARIOINGRESADO->getTipo() != 'E') { ?>
                <input type="text" class="form-control" name="nit" value="<?= $empresa->getNit() ?>">
                <?php } ?>
            </div>

            <label for="" class="col-sm-1 col-form-label">Nombre:</label>
            <div class="col-sm-3 mb-2">
                <input type="text" class="form-control" name="nombre" value="<?= $empresa->getNombre() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Telefono:</label>
            <div class="col-sm-3 mb-2">
                <input type="number" class="form-control"   name="telefono" value="<?= $empresa->getTelefono() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Email:</label>
            <div class="col-sm-3 mb-3">
                <input type="email" class="form-control"   name="email" value="<?= $empresa->getEmail() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Dirección:</label>
            <div class="col-sm-3 mb-3">
                <input type="text" class="form-control"   name="direccion" value="<?= $empresa->getDireccion() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label" <?= $ocultar ?><?= $ocultar1 ?> >Tipo</label>
            <div class="col-sm-3 mb-2" <?= $ocultar ?><?= $ocultar1 ?>>
                <select class="form-control" name="tipo" id="tipo" >
                    <?= $auxiliar ?>
                </select>
            </div>

            <label for="" class="col-sm-1 col-form-label">Pais</label>
            <div class="col-sm-2 mb-2">
                <select class="form-control" name="codpais" id="codpais" onChange="cargarDepartamentos()">
                    <?= Pais::getListaInOptions($pais->getCodigo()) ?>
                </select>
            </div>

            <label for="" class="col-sm-2 col-form-label">Departamento</label>
            <div class="col-sm-3 mb-2">
                <select class="form-control" name="coddepartamento" id="coddepartamento" onchange="cargarCiudades(this.value)">

                </select>
            </div>

            <label for="" class="col-sm-1 col-form-label">Ciudad</label>
            <div class="col-sm-3 mb-2">
                <select class="form-control" name="codciudad" id="codciudad">
                </select>
            </div>
        </div>


        <h2>DATOS DEL ADMINISTRADOR</h2><br>

        <div class="form-group row">
            <label for="" class="col-sm-1 col-form-label">Identificacion:</label>
            <div class="col-sm-3 mb-2">
                <input type="number" class="form-control"   name="identificacion" value="<?= $persona->getIdentificacion() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Nombres:</label>
            <div class="col-sm-3 mb-2">
                <input type="text" class="form-control" name="nombres"value="<?= $persona->getNombres() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Apellidos:</label>
            <div class="col-sm-3 mb-2">
                <input type="text" class="form-control"   name="apellidos" value="<?= $persona->getApellidos() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Telefono:</label>
            <div class="col-sm-3 mb-3">
                <input type="number" class="form-control"   name="telefonoPersona" value="<?= $persona->getTelefono() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Email:</label>
            <div class="col-sm-3 mb-3">
                <input type="email" class="form-control"   name="emailPersona" value="<?= $persona->getEmail() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label" ">Clave:</label>
            <div class="col-sm-3 mb-3">
                <input type="password" class="form-control"   name="clave" value="<?= $persona->getClave() ?>">
            </div>

        </div>
        <input type="hidden" name="id" value="<?= $empresa->getId() ?>">
        <input type="hidden" name="idPersona" value="<?= $persona->getIdentificacion() ?>">
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary mb-2" name="accion" value="<?= $accion ?>"><?= $accion ?></button>
        </div>

    </form>
</div>

<script type="text/javascript">
    var departamentos = new Array();
    var ciudades = new Array();

<?php
$departamentos = Departamento::getListaInObject(null, "codPais, nombre");
$listaDepartamentos = "";
for ($i = 0; $i < count($departamentos); $i++) {
    $departamento = $departamentos[$i];
    $listaDepartamentos .= "\tdepartamentos[$i] = new Array();\n";
    $listaDepartamentos .= "\tdepartamentos[$i][0] = '{$departamento->getCodigo()}';\n";
    $listaDepartamentos .= "\tdepartamentos[$i][1] = '{$departamento->getNombre()}';\n";
    $listaDepartamentos .= "\tdepartamentos[$i][2] = '{$departamento->getCodigoPais()}';\n";
}

$ciudades = Ciudad::getListaInObject(null, "codDepartamento, nombre");
$listaCiudades = "";
for ($i = 0; $i < count($ciudades); $i++) {
    $ciudad = $ciudades[$i];
    $listaCiudades .= "ciudades[$i]=new Array();\n";
    $listaCiudades .= "ciudades[$i][0]='{$ciudad->getCodigo()}';\n";
    $listaCiudades .= "ciudades[$i][1]='{$ciudad->getNombre()}';\n";
    $listaCiudades .= "ciudades[$i][2]='{$ciudad->getCodigoDepartamento()}';\n";
}
?>
<?= $listaDepartamentos ?>
<?= $listaCiudades ?>
    cargarDepartamentos(<?= $departamentoPrincipal->getCodigo() ?>);
    cargarCiudades(<?= $ciudadPrincipal->getCodigo() ?>)

    function cargarDepartamentos(predeterminado) {
        formulario.coddepartamento.length = 0;
        for (var i = 0; i < departamentos.length; i++) {
            if (formulario.codpais.value == departamentos[i][2]) {
                formulario.coddepartamento.length++;
                formulario.coddepartamento.options[formulario.coddepartamento.length - 1].value = departamentos[i][0];
                formulario.coddepartamento.options[formulario.coddepartamento.length - 1].text = departamentos[i][1];

                if (predeterminado == departamentos[i][0])
                    formulario.coddepartamento.options[formulario.coddepartamento.length - 1].selected = true;
            }
        }
        cargarCiudades(formulario.coddepartamento.value, null);
    }

    function cargarCiudades(predeterminado) {
        formulario.codciudad.length = 0;
        for (var i = 0; i < ciudades.length; i++) {
            if (formulario.coddepartamento.value == ciudades[i][2]) {
                formulario.codciudad.length++;
                formulario.codciudad.options[formulario.codciudad.length - 1].value = ciudades[i][0];
                formulario.codciudad.options[formulario.codciudad.length - 1].text = ciudades[i][1];
                if (predeterminado == ciudades[i][0])
                    formulario.codciudad.options[formulario.codciudad.length - 1].selected = true;
            }
        }
    }

</script>