<?php

require_once 'clases/Factura.php';
require_once 'clases/FacturaDetalle.php';
require_once 'clases/Persona.php';

require_once 'clases/Empresa.php';


require_once 'clases/Sucursal.php';
$impresion = '';
$suma = 0;
date_default_timezone_set('America/Bogota');
$lista = '';



if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='entregar'){
    echo 'si entre';
    print_r($_REQUEST);
}







if(isset($_GET['id'])){
    $factura = new Factura($_GET['id']);
    
    $boton = 'disabled';
    $cliente = new Persona($factura->getIdentificacionCliente());
    $readonly = 'readonly';
    
    $resultado = Factura::getLista(' producto.nombre as nombreproducto, facturaproducto.cantidad as cantidad, 
facturaproducto.valorunitario as valorunitario, 
facturaproducto.iva as iva, (facturaproducto.cantidad *facturaproducto.valorunitario) + facturaproducto.valorunitario 
+ (facturaproducto.valorunitario*facturaproducto.iva/100) as subtotal
from factura join facturaproducto on factura.id = facturaproducto.idfactura 
join producto on producto.id = facturaproducto.idproducto where idfactura = '.$_GET['id']);

    foreach ($resultado as $datos){
        $lista .= "<tr>";
        $lista .= "<td>{$datos['nombreproducto']}<input type='hidden' value='{$datos['id']}' name[]='id' > </td>";
        $lista .= "<td>{$datos['cantidad']}</td>";
        $lista .= "<td>{$datos['valorunitario']}</td>";
        $lista .= "<td>{$datos['iva']}</td>";
        $lista .= "<td>{$datos['subtotal']}</td>";
        $lista .= "<td></td>";
        $lista .= "<tr>";
        $suma+=$datos['subtotal'];
    }


}else{
    $factura = new Factura(null);
    $cliente = new Persona(null);
    $boton='';

}

?>

<style>
    .autocomplete-suggestions{
        border: 2px solid #2E86C1 ; background: #F7F9F9; overflow: auto;
    }
</style>

<div class="container-fluid">

    <div class="col-12">
        <h3>REGISTRAR VENTA</h3>
        <form name="formulario" action="principal.php?contenido=entregar.php" method="post" >
            <input type="hidden" value="<?= $_REQUEST['id'] ?>">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Fecha</label>
                    <label class="form-control"><?= $factura->getFecha() ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4">Sucursal</label>
                    <!-- <input type="number" required class="form-control" id="inputPassword4"  value=""> -->
                    <label class="form-control"><?= new Empresa($factura->getIdSucursal()) ?></label>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4">Vendedor</label>
                    <label class="form-control"><?=$USUARIOINGRESADO?></label>
                </div>
            </div>
            <h3>DATOS DEL CLIENTE</h3>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Identificacion</label>
                    <label class="form-control"><?= $cliente->getIdentificacion() ?></label>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Nombre</label>
                    <label class="form-control"><?= $cliente->getNombres() ?></label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Apellido</label>
                    <label class="form-control"><?= $cliente->getApellidos() ?></label>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Telefono</label>
                    <label class="form-control"><?= $cliente->getTelefono() ?></label>
                </div>
            </div>

            <div class="form-row">
                <div class="table-responsive">
                    <table id="miTabla" class="table table-hover">
                        <thead>
                        <tr>
                            <?php
                                if(!isset($_GET['id'])){


                            ?>
                            <th>Id</th>
                            <?php
                                }
                            ?>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Valor unitario</th>
                            <th>IVA</th>
                            <th>Subtotal</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?= $lista ?>
                        </tbody>
                    </table>

                </div>
                <div class="ml-auto">
                    <p><h1 id="total">Total: $<?=$suma?></h1></p>
                </div>
            </div>

            <input type="hidden" name="id" value="">
            <button type="submit" name="accion" value="entregar" class="btn btn-primary">Entregar</button>
        </form>
        <div class="row">
            <div class="col-12 text-center ">
                <?= $impresion ?>
            </div>
        </div>
        <br>
        <br>
    </div>
</div>