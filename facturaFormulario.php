<?php

require_once 'clases/Factura.php';
require_once 'clases/FacturaDetalle.php';
require_once 'clases/Persona.php';

require_once 'clases/Empresa.php';


require_once 'clases/Sucursal.php';
$impresion = '';
$suma = 0;
date_default_timezone_set('America/Bogota');
$lista = '';

if(isset($_POST['accion']) && $_POST['accion']=='guardar'){

    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';


    $personaEnviada=new Persona($_REQUEST['identificacioncliente']);

    if($personaEnviada->getNombres()!=''){//modificar usuario
        $personaEnviada->setNombres($_POST['nombrecliente']);
        $personaEnviada->setApellidos($_POST['apellidocliente']);
        $personaEnviada->setTelefono($_POST['telefonocliente']);
        $personaEnviada->modificarFactura();
    }else {//adicionar usuario
        $personaEnviada->setIdentificacion($_POST['identificacioncliente']);
        $personaEnviada->setNombres($_POST['nombrecliente']);
        $personaEnviada->setApellidos($_POST['apellidocliente']);
        $personaEnviada->setTelefono($_POST['telefonocliente']);
       // $personaEnviada->modificar($_POST['identificacionclienteoculto']);
        $personaEnviada->guardar();
    }

    // if($_POST['identificacionclienteoculto'] != ''){
    //     $cliente = new Persona($_POST['identificacionclienteoculto']);
    //     $cliente->setIdentificacion($_POST['identificacioncliente']);
    //     $cliente->setNombres($_POST['nombrecliente']);
    //     $cliente->setApellidos($_POST['apellidocliente']);
    //     $cliente->setTelefono($_POST['telefonocliente']);
    //     $cliente->modificar($_POST['identificacionclienteoculto']);
    // }else{
    //     $cliente = new Persona(null);
    //     $cliente->setIdentificacion($_POST['identificacioncliente']);
    //     $cliente->setNombres($_POST['nombrecliente']);
    //     $cliente->setApellidos($_POST['apellidocliente']);
    //     $cliente->setTelefono($_POST['telefonocliente']);
    //     $cliente->setTipo('U');
    //     $cliente->guardar();
    // }

    $factura = new Factura(null);
    $factura->setFechaEntrega('');
    $factura->setIdentificacionCliente($personaEnviada->getIdentificacion());
    $factura->setIdSucursal($_REQUEST['idsucursal']);
    $factura->setEstadoFactura('P');
    $factura->setIdVendedor($USUARIOINGRESADO->getIdentificacion());
    $factura->setIdentificacionCliente($_REQUEST['identificacioncliente']);
    $factura->guardar();

    $facturaProducto = new FacturaDetalle(null);
    for ($i = 0; $i<count($_POST['idproducto']); $i++){
        $facturaProducto->setIdFactura($factura->getId());
        $facturaProducto->setIdProducto($_POST['idproducto'][$i]);
        $facturaProducto->setCantidad($_POST['cantidadproducto'][$i]);
        $facturaProducto->setValorunitario($_POST['valorunitario'][$i]);
        $facturaProducto->setIva($_POST['iva'][$i]);
        $facturaProducto->guardar();
    }

}

if(isset($_GET['id'])){
    $factura = new Factura($_GET['id']);
    var_dump($factura);
    $boton = 'disabled';
    $cliente = new Persona($factura->getIdentificacionCliente());
    $readonly = 'readonly';
    var_dump($cliente);
    $resultado = Factura::getLista(' producto.nombre as nombreproducto, facturaproducto.cantidad as cantidad, 
facturaproducto.valorunitario as valorunitario, 
facturaproducto.iva as iva, (facturaproducto.cantidad *facturaproducto.valorunitario) + facturaproducto.valorunitario 
+ (facturaproducto.valorunitario*facturaproducto.iva/100) as subtotal
from factura join facturaproducto on factura.id = facturaproducto.idfactura 
join producto on producto.id = facturaproducto.idproducto where idfactura = '.$_GET['id']);

    foreach ($resultado as $datos){
        $lista .= "<tr>";
        $lista .= "<td>{$datos['nombreproducto']}</td>";
        $lista .= "<td>{$datos['cantidad']}</td>";
        $lista .= "<td>{$datos['valorunitario']}</td>";
        $lista .= "<td>{$datos['iva']}</td>";
        $lista .= "<td>{$datos['subtotal']}</td>";
        $lista .= "<td></td>";
        $lista .= "<tr>";
        $suma+=$datos['subtotal'];
    }


}else{
    $readonly = '';
    $factura = new Factura(null);
    $cliente = new Persona(null);
    $boton='';

}

?>

<style>
    .autocomplete-suggestions{
        border: 2px solid #2E86C1 ; background: #F7F9F9; overflow: auto;
    }
</style>

<div class="container-fluid">

    <div class="col-12">
        <h3>REGISTRAR VENTA</h3>
        <form name="formulario" action="principal.php?contenido=facturaFormulario.php" method="post" >
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Fecha</label>
                    <input readonly type="text" required class="form-control" id="inputEmail4" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4">Sucursal</label>
                    <!-- <input type="number" required class="form-control" id="inputPassword4"  value=""> -->
                    <select  <?= (($USUARIOINGRESADO->getTipo()!='S') ? 'readonly' : '') ?> required class="form-control" id="inputPassword4"  name="idsucursal" >
                        <?= Sucursal::getListaEnOpciones( (($USUARIOINGRESADO->getTipo()=='A') ? $_REQUEST['idempresa'] : $USUARIOINGRESADO->getIdEmpresa()), $USUARIOINGRESADO->getIdSucursal()) ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4">Vendedor</label>
                    <input readonly type="text" required class="form-control" id="inputEmail4" name="nombre" value="<?=$USUARIOINGRESADO?>">
                </div>
            </div>
            <h3>DATOS DEL CLIENTE</h3>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Identificacion</label>
                    <input type="hidden" required class="form-control" id="identificacionclienteoculto" name="nombreclienteoculto" value="">
                    <input type="text" <?=$readonly?> required class="form-control" id="identificacioncliente" name="identificacioncliente" value="<?= $cliente->getIdentificacion() ?>" >
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Nombre</label>
                    <input type="text" <?=$readonly?>  class="form-control" id="nombrecliente" name="nombrecliente" value="<?= $cliente->getNombres() ?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Apellido</label>
                    <input type="text" <?=$readonly?> required class="form-control" id="apellidocliente" name="apellidocliente" value="<?= $cliente->getApellidos() ?>">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Telefono</label>
                    <input type="text" <?=$readonly?> required class="form-control" id="telefonocliente" name="telefonocliente" value="<?= $cliente->getTelefono() ?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="inputPassword4">Producto</label>
                    <input type="text" <?=$readonly?> class="form-control" id="autocomplete-input" autocomplete='on' name="nombre" value="">
                </div>

            </div>

            <div class="form-row">
                <div class="table-responsive">
                    <table id="miTabla" class="table table-hover">
                        <thead>
                        <tr>
                            <?php
                                if(!isset($_GET['id'])){


                            ?>
                            <th>Id</th>
                            <?php
                                }
                            ?>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Valor unitario</th>
                            <th>IVA</th>
                            <th>Subtotal</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?= $lista ?>
                        </tbody>
                    </table>

                </div>
                <div class="ml-auto">
                    <p><h1 id="total">Total: $<?=$suma?></h1></p>
                </div>
            </div>

            <input type="hidden" name="id" value="">
            <button type="submit" name="accion" value="guardar" <?= $boton?> class="btn btn-primary">Guardar factura</button>
        </form>
        <div class="row">
            <div class="col-12 text-center ">
                <?= $impresion ?>
            </div>
        </div>
    </div>

</div>
<script>
    ////////////////////////////////////////
    $('#identificacioncliente').focusout(function () {
        var parametro = $('#identificacioncliente').val();
        var datos = {
            "identificacion": parametro
        };
        $.ajax({
            type: "POST",
            url: 'ajaxphp/obtenercliente.php',
            data: datos,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if(data.identificacion != null){
                    $('#identificacionclienteoculto').val(data.identificacion);
                    $('#identificacioncliente').val(data.identificacion);
                    $('#nombrecliente').val(data.nombre);
                    $('#apellidocliente').val(data.apellido);
                    $('#telefonocliente').val(data.telefono);
                    if (data.tipo!='U'){
                        $('#identificacioncliente').prop('readonly', true);
                        $('#nombrecliente').prop('readonly', true);
                        $('#apellidocliente').prop('readonly', true);
                        $('#telefonocliente').prop('readonly', true);
                    }
                }else{
                    alert("No se encuentra registro de este cliente en el sistema, se procedera a registrar");
                    $('#identificacionclienteoculto').val("");
                    $('#nombre').val("");
                    $('#apellido').val("");
                    $('#telefono').val("");
                }
            },
            error: function () {
                alert("Error al consultar un cliente");
            }
        });
    });
    ///////////////////////////////////////////
    //CODIGO PARA AUTOCOMPLETAR, AQUI SE AUTOCOMPLETA, Y AL MOMENTO DE SELECCIONAR EL ITEM ESTE AUTOMATICAMENTE SE AÑADE A LA TABLA
    $('#autocomplete-input').autocomplete({
        onSearchStart: function (params) {
            params.Enviado = $('#autocomplete-input').val();
        },
        serviceUrl: 'ajaxphp/listaProductos.php',
        onSelect: function (suggestion) {
            let codigo='';
            codigo = $('#miTabla td:contains(id-p'+suggestion.data[0]+')').text().toString();
            if(codigo===''){
                $('#miTabla').append("<tr>" +
                    "<td><input type='hidden' name='idproducto[]' value='"+suggestion.data[0]+"'>id-p"+suggestion.data[0]+"</td>" +
                    "<td>"+suggestion.value+"</td>" +
                    "<td><input class='itemcantidad' type='number' name='cantidadproducto[]' value='1' min='1' max='"+suggestion.data[2]+"' ></td>" +
                    "<td><input type='hidden' name='valorunitario[]' value='"+suggestion.data[5]+"'>"+suggestion.data[5]+"</td>" +
                    "<td><input type='hidden' name='iva[]' value='"+suggestion.data[6]+"'>"+suggestion.data[6]+"</td>" +
                    "<td><input type='hidden' name='subtotal[]' value='"+suggestion.data[5]+"'>"+ parseInt(suggestion.data[5]+(suggestion.data[5]*suggestion.data[6]/100)) +"</td>" +
                    "<td><i class='fa fa-eraser borrarItem' title='Eliminar'></i></td>" +
                    "</tr>");
            }
            $('#autocomplete-input').val("");
            cargarTotal();
        }
    });
    //CALCULA CADA VEZQUE SE AUMENTE LA CANTIDAD DE LOS PRODUCTOS, EL SUBTOTAL
    $('#miTabla').on('keyup mouseup', '.itemcantidad', function () {
        let valorUnitario = parseInt($(this).closest('tr').find('td:eq(3) input').val());
        let iva = parseInt($(this).closest('tr').find('td:eq(4) input').val());
        let valorConIva = valorUnitario + (valorUnitario*iva/100);
        let subtotal = $(this).val() * valorConIva;
        $(this).closest('tr').find('td:eq(5)').html("<input type='hidden' name='subtotal[]' value='"+subtotal+"'>"+subtotal+"  ");
        cargarTotal();
    });

    //BORRA LOS ITEMS QUE ESTAN AGREGADOS EN LA TABLA
    $('#miTabla').on('click', '.borrarItem', function () {
        $(this).closest('tr').remove();
        cargarTotal();
    });

    //MUESTRA EL TOTAL EN UNA ETIQUETA HTML
    function cargarTotal() {

        let tds = document.querySelectorAll('#miTabla tbody td');
        let total = 0;
        let h = document.getElementById("total");
        for (var i = 0; i < tds.length; i++) {

            if(tds[i].cellIndex == '5'){
                total = total + parseInt(tds[i].innerText);
            }
        }
        h.innerText = "Total: $"+total;
    }



</script>
