<?php
    include_once './clases/Empresa.php';
    include_once './clases/Sucursal.php';
    include_once './clases/Producto.php';
    include_once './clases/TipoPersona.php';


    if($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='E') $idEmpresa=$USUARIOINGRESADO->getIdEmpresa(); 
    else $idEmpresa=$_REQUEST['idempresa'];
    $empresa=new Empresa($idEmpresa);

    if(isset($_POST['cargar'])){ //cuando se a precionado enviar en el formulario
        $rutaArchivo = $_FILES['archivo']['tmp_name'];//ruta del archivo subido
        $archivo = fopen($rutaArchivo, "r");//abre el archivo y lo citua en la primera linea 
        $i=0;
        $importar='';
    
        while( ($fila = fgetcsv($archivo, 10000, ";") ) !== FALSE ){//recorre el archivo con un maximo de 10000 caracteres cada campo
            $producto=new Producto(null);//declarar objeto
            //inicio ingreso de datos
            $producto->setNombre($fila[0]);
            $producto->setStock($fila[1]);
            $producto->setStockMinimo($fila[2]);
            $producto->setStockMaximo($fila[3]);
            $producto->setStockInicial($fila[4]);
            $producto->setValorUnitario($fila[5]);
            $producto->setIva($fila[6]);
            $producto->setIdEmpresa($fila[7]);
            $producto->setDisponible($fila[8]);
            $producto->setIdProveedor($fila[9]);
            $producto->guardar();
            //fin ingreso de datos y guardado
            //print_r($producto);
        }
        
    }

?>
<div class="col-12">
    <h3>IMPORTAR PRODUCTOS PARA LA EMPRESA <?= strtoupper($empresa) ?></h3>
    <br>
    <div class="row">
        <form method="POST" action="principal.php?contenido=importar.php" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleFormControlFile1">Seleccione un archivo</label>
                <input type="file" class="form-control-file" name="archivo">
            </div>
            <button type="submit" class="btn btn-primary" name="cargar" value="cargar">Cargar</button>
        </form>
    </div>
</div>

