<!DOCTYPE html>
<?php
    session_start();
    session_status();
    session_destroy();
    $mensaje = "";
    if (isset($_REQUEST['mensaje'])) $mensaje = $_REQUEST['mensaje']; 
?>
<html lang="en">
    <head>
        <!--Required meta tags-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Warsoft</title>
        <!--Bootstrap CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="js\jquery-3.5.1.min.js"></script>
        <script src="js\jquery.autocomplete.js"></script>
    </head>
    <body>
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <form method="POST" action="validar.php">
                    <h1 class="">WareSoft</h1>
                    <center><p class="error"><?=$mensaje?></p></center>
                    <div class="form-group">
                        <label>Usuario</label>
                        <input type="number" name="identificacion" class="form-control" placeholder="Usuario/identificaicón">
                    </div>
                    <div class="form-group">
                        <label >Clave</label>
                        <input type="password" name="clave" class="form-control" placeholder="Clave">
                    </div>
                    <button type="submit" class="btn btn-primary">Ingresar</button>
                </form>
            </div>
        </div>
        <!--Optional JavaScript-->
        <!--jQuery first, then Popper.js, then Bootstrap JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>