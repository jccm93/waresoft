<?php
require_once 'clases/Persona.php';

@session_start();

    $menu='';
    $scriptImprimir='';
    switch ($USUARIOINGRESADO->getTipo()) {
        case 'A':
            $menu="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=empresa.php'>Empresa</a></li>";
        break;
        case 'E':
            $menu="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=empresa.php'>Empresa</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=colaboradores.php'>Colaborador</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=producto.php'>Productos</a></li>";
            //$menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=productoProveedor.php'>Productos proveedor</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=facturas.php'>Facturas</a></li>";
        break;
        case 'S':
            $menu="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=empresa.php'>Empresa</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=colaboradores.php'>Colaborador</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=producto.php'>Productos</a></li>";
            //$menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=productoProveedor.php'>Productos proveedor</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=facturas.php'>Facturas</a></li>";
            $scriptImprimir="<script>
setInterval(() => {
    $.ajax({
        type: 'POST',
        url: 'ajaxphp/notificaciones.php',
        data: {opcion: '1'},
        success: function (datos) {
            console.log(datos);
            if(datos!=null){
                datos=JSON.parse(datos)
                console.log(datos);
                document.getElementById('cantidad').innerHTML=datos.length;
                let contenidoAgregar='';
                for (let i = 0; i < datos.length; i++) {
                    contenidoAgregar+=\"<a class='dropdown-item' href='principal.php?contenido=productoFormulario.php&id=\"+datos[i]['id']+\"&entrada=t'>Poco stock de \"+datos[i]['nombre']+\"</a>\";
                }
                console.log(contenidoAgregar);
                document.getElementById('contenidonotificacion').innerHTML=contenidoAgregar;
            }
        },
        error: function () {
            alert(\"Error al consultar\");
        }
    });
}, 1000);

</script>
";
        break;
        case 'V':
            $menu="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=facturas.php'>Facturas</a></li>";
        break;
        case 'B':
            $menu="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=empresa.php'>Empresa</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=producto.php'>Productos</a></li>";
            $menu.="<li class='nav-item'><a class='nav-link' href='principal.php?contenido=facturas.php'>Facturas</a></li>";
            $scriptImprimir="<script>
setInterval(() => {
    $.ajax({
        type: 'POST',
        url: 'ajaxphp/notificaciones.php',
        data: {opcion: '1'},
        success: function (datos) {
            if(datos!=null){
                datos=JSON.parse(datos)
                document.getElementById('cantidad').innerHTML=datos.length;
                let contenidoAgregar='';
                for (let i = 0; i < datos.length; i++) {
                    contenidoAgregar+=\"<a class='dropdown-item' href='principal.php?contenido=productoFormulario.php&id=\"+datos[i]['id']+\"&entrada=t'>Poco stock de \"+datos[i]['nombre']+\"</a>\";
                }
                console.log()
                document.getElementById('contenidonotificacion').innerHTML=contenidoAgregar;
            }
        },
        error: function () {
            alert(\"Error al consultar\");
        }
    });
}, 10000);

</script>
";
        break;
    }
    echo $menu;
?>

<li class='nav-item'>
    <a class='nav-link' href='principal.php?contenido=acercaDe.php'>Acerca de</a>
</li>
<li class='nav-item'>
    <a class='nav-link' href='salir.php'>Salir</a>
</li>

<li>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <b ><i class="far fa-bell"></i><span class="badge badge-light" id="cantidad"></span></b>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="contenidonotificacion">
            
        </div>
    </div>
</li>

<?= $scriptImprimir ?>