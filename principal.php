<?php
    include_once './clasesGenericas/ConectorBD.php';
    include_once './clases/Persona.php';
    $CONTENIDO='blanco.php';

    session_start();

    if(isset($_SESSION['usuario'])){
        $USUARIOINGRESADO=$_SESSION['usuario'];
    }

    include_once './validador.php';

    if(isset($_REQUEST['contenido'])){
        $CONTENIDO=$_REQUEST['contenido'];
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!--Required meta tags-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>WereSoft</title>
        <!--Bootstrap CSS-->
        <link rel="stylesheet" href="fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="js/jquery-3.5.1.min.js"></script>
        <script src="js/jquery.autocomplete.js"></script>
    </head>
    <body>
        <div class="container-fluid ">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <!--tips: to change the nav placement use .fixed-top,.fixed-bottom,.sticky-top-->
                <a class="navbar-brand" href="index.php">WareSoft</a>
                <!--<a class="navbar-brand" href="#">
                    <img src="..." class="d-inline-block align-top" width="30" height="30" alt="...">My Brand
                </a>-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto">
                        <?php include_once './menu.php' ?>
                    </ul>
                </div>
            </nav>
            <div class="container">
                <div class="row">
                    <?php require_once $CONTENIDO ?>
                </div>
            </div>
        </div>

        <!--Optional JavaScript-->
        <!--jQuery first, then Popper.js, then Bootstrap JS-->
        <script src="js/jquery-3.5.1.min.js"></script>
        <script src="js/jquery.autocomplete.js"></script>
        <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>