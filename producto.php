<?php
    include_once './clases/Producto.php';

    $lista='';
    $filtro='';

    $codigo='';
    $stockInicio='';
    $stockFin='';

    if($USUARIOINGRESADO->getTipo()!='A') $idEmpresa=$USUARIOINGRESADO->getIdEmpresa(); 
    else $idEmpresa=$_REQUEST['idempresa'];

    $filtro=' where producto.idempresa = '.$idEmpresa;
    
    if(isset($_POST['codigo']) || isset($_POST['stock']) || isset($_POST['codigo'])){
        
        if($_POST['codigo']){
            $filtro.=" and producto.id::text like '%{$_POST['codigo']}%' ";
            $codigo=$_REQUEST['codigo'];
        }

        if($_POST['stockinicio']){
            $stockInicio=$_REQUEST['stockinicio'];
            if($_POST['stockfin']){
                $filtro.=" and stock between {$_POST['stockinicio']} and {$_REQUEST['stockfin']} ";
                $stockFin=$_REQUEST['stockfin'];
            }else {
                $filtro.=" and stock >= {$_POST['stockinicio']} ";
            }
        }else {
            if($_POST['stockfin']){
                $filtro.=" and stock <= {$_REQUEST['stockfin']} ";
                $stockFin=$_REQUEST['stockfin'];
            }
        }
    }


    $resultado=Producto::getLista(' producto.id, producto.nombre, stock, stockminimo, stockmaximo, stockinicial, valorunitario, producto.iva, disponible, empresa.nombre as proveedor from producto join empresa on empresa.id=producto.idproveedor '.$filtro.' order by id');
    
    foreach ($resultado as $key => $datos) {
        $lista.="<tr>";
        $lista.="<td>{$datos['id']}</td>";
        $lista.="<td>{$datos['nombre']}</td>";
        $lista.="<td>{$datos['stockinicial']}</td>";
        $lista.="<td>{$datos['stock']}</td>";
        $lista.="<td>{$datos['stockminimo']}</td>";
        $lista.="<td>{$datos['stockmaximo']}</td>";
        $lista.="<td>{$datos['valorunitario']}</td>";
        $lista.="<td>{$datos['iva']}</td>";
        $lista.="<td>{$datos['proveedor']}</td>";
        $lista.=($USUARIOINGRESADO->getTipo()!='B' ? "<td><a href='principal.php?contenido=productoFormulario.php&id={$datos['id']}' ><i class='fas fa-edit'></i></a><a href='principal.php?contenido=productoFormulario.php&accion=Eliminar&id={$datos['id']}' ><i class='fas fa-trash'></i></a>".($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='B' ? "<a title='Ingresar producto' href='principal.php?contenido=productoFormulario.php&id={$datos['id']}&entrada=t' ><i class='fas fa-truck-moving'></i></a>":'').'</td>': '');
        $lista.="</tr>";
    }

?>
<div class="col-12">
    <h3>LISTA PRODUCTOS</h3>
    <br>
    <div class="row">
        <form name="formulario" method="POST" action="principal.php?contenido=producto.php">
            <div class="form-group row">
                <label for="" class="col-sm-1 col-form-label">Codigo:</label>
                <div class="col-sm-2 mb-2">
                    <input type="text" class="form-control"   name="codigo" value="<?= $codigo ?>">
                </div>
                
                <label for="" class="col-sm-1 col-form-label">Stock&nbsp;inicial:</label>
                <div class="col-sm-2 mb-2">
                    <input type="number" class="form-control" name="stockinicio" value="<?= $stockInicio ?>">
                </div>

                <label for="" class="col-sm-1 col-form-label">hasta:</label>
                <div class="col-sm-2 mb-2">
                    <input type="number" class="form-control" name="stockfin" value="<?= $stockFin ?>">
                </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary mb-2">Buscar</button>
        </div>
    </form>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad inicial</th>
                        <th>Stock</th>
                        <th>Stock minimo</th>
                        <th>Stock maximo</th>
                        <th>Valor unitario</th>
                        <th>Iva</th>
                        <th>Proveedor</th>
                        <th>Gestion <?= ($USUARIOINGRESADO->getTipo()!='B' ? "<a title=\"Añadir nuevo producto\" href=\"principal.php?contenido=productoFormulario.php\"><i class=\"fas fa-plus\"></i></a> <a title=\"Importar productos de csv\" href=\"principal.php?contenido=importar.php\"><i class=\"far fa-file-excel\"></i></a>" : ''); ?> </th>
                    </tr>
                </thead>
                <tbody>
                    <?=$lista?>
                </tbody>
            </table>
        </div>
    </div>
</div>