<?php
    include_once './clases/Producto.php';
    include_once './clases/Empresa.php';
    include_once './clases/EstadoProducto.php';

    if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Adicionar'){
        $_REQUEST['stockinicial']=$_REQUEST['stock'];
        $_REQUEST['idempresa']=$USUARIOINGRESADO->getIdEmpresa();
        $producto=new Producto($_REQUEST);
        $producto->guardar();
        //header("Location: principal.php?contenido=producto.php");
    }else if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Modificar'){

        if(isset($_REQUEST['cantidad']) && $_REQUEST['cantidad']>0) $_REQUEST['stockinicial']=$_REQUEST['stockinicial']+$_REQUEST['cantidad'];
        $_REQUEST['idempresa']=$USUARIOINGRESADO->getIdEmpresa();
        $producto=new Producto($_REQUEST);
        $producto->modificar();
        header("Location: principal.php?contenido=producto.php");
    }else if(isset($_REQUEST['accion']) && $_REQUEST['accion']=='Eliminar'){;
        $producto=new Producto(null);
        $producto->setId($_REQUEST['id']);
        $producto->eliminar();
        header("Location: principal.php?contenido=producto.php");
    }

    $accion='Adicionar';
    if(isset($_REQUEST['id'])){
        $producto=new Producto($_REQUEST['id']);
        $accion='Modificar';
    }else $producto=new Producto(null);
    $editarMenu='';
    if(isset($_REQUEST['entrada']) && $_REQUEST['entrada']=='t'){
        $editarMenu='readonly';
    }

?>
<div class="col-12">
    <h2>ADICIONAR PRODUCTO</h2><br>
    <form name="formulario" method="POST" action="principal.php?contenido=productoFormulario.php">
        <div class="form-group row">
            <label for="" class="col-sm-1 col-form-label">Nombre:</label>
            <div class="col-sm-2 mb-2">
                <input <?=$editarMenu?> type="text" class="form-control" name="nombre" value="<?= $producto->getNombre() ?>">
            </div>
            
            <label for="" class="col-sm-1 col-form-label">Stock:</label>
            <div class="col-sm-2 mb-2">
                <input <?=$editarMenu?> type="text" class="form-control" name="stock" value="<?= $producto->getStock() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Stock&nbsp;minimo:</label>
            <div class="col-sm-2 mb-2">
                <input <?=$editarMenu?> type="number" class="form-control" name="stockminimo" value="<?= $producto->getStockMinimo() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Stock&nbsp;maximo:</label>
            <div class="col-sm-2 mb-3">
                <input <?=$editarMenu?> type="number" class="form-control" name="stockmaximo" value="<?= $producto->getStockMaximo() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Valor&nbsp;unitario:</label>
            <div class="col-sm-2 mb-3">
                <input <?=$editarMenu?> type="text" class="form-control" name="valorunitario" value="<?= $producto->getValorUnitario() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Iva:</label>
            <div class="col-sm-2 mb-3">
                <input <?=$editarMenu?> type="text" class="form-control" name="iva" value="<?= $producto->getIva() ?>">
            </div>

            <label for="" class="col-sm-1 col-form-label">Proveedor</label>
            <div class="col-sm-2 mb-2">
                <select <?=$editarMenu?> class="form-control" name="idproveedor">
                    <?= Empresa::getProveedoresEnOpciones($producto->getIdEmpresa()); ?>
                </select>
            </div>

            <label for="" class="col-sm-1 col-form-label">Estado</label>
            <div class="col-sm-2 mb-2">
                <select <?=$editarMenu?> class="form-control" name="disponible">
                        <?= EstadoProducto::getEstadoProductoEnOpciones($producto->getDisponible()) ?>
                </select>
            </div>

            

            <?= (isset($_GET['id']) && isset($_GET['entrada']) && $_GET['entrada']=='t' && ($USUARIOINGRESADO->getTipo()=='S' || $USUARIOINGRESADO->getTipo()=='B') ? "<label class='col-sm-2 col-form-label'>Cantidad entrante:</label>
            <div class='col-sm-3 mb-3'>
                <input type='number' class='form-control' name='cantidad' value='0'>
            </div>" : '') ?>
            
        <div class="form-group text-center">
            <input type="hidden" name="stockinicial" value="<?= $producto->getStockInicial() ?>">
            <input type="hidden" name="id" value="<?= $producto->getId() ?>">
            <button type="submit" class="btn btn-primary mb-2" name="accion" value="<?= $accion ?>"><?= $accion ?></button>
        </div>
        
    </form>
</div>