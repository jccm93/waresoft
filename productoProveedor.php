<div class="col-12">
    <h3>LISTA PRODUCTOS DE LA EMPRESA XYXX</h3>
    <br>
    <div class="row">
        <form name="formulario" method="POST" action="">
            <div class="form-group row">
                <label for="" class="col-sm-1 col-form-label">Codigo:</label>
                <div class="col-sm-2 mb-2">
                    <input type="text" class="form-control"   name="codigo">
                </div>

                <label for="" class="col-sm-1 col-form-label">Productos:</label>
                <div class="col-sm-2 mb-2">
                    <select class="form-control" name="idempresa">
                            <option>Terminados</option>
                            <option>Escasos</option>
                    </select>
                </div>
                
                <label for="" class="col-sm-1 col-form-label">Stock:</label>
                <div class="col-sm-2 mb-2">
                    <input type="number" class="form-control" name="stock">
                </div>         
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary mb-2">Guardar</button>
        </div>
        
    </form>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad inicial</th>
                        <th>Stock</th>
                        <th>Stock minimo</th>
                        <th>Stock maximo</th>
                        <th>Valor unitario</th>
                        <th>Iva</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>