<?php
require_once './clasesGenericas/ConectorBD.php';
require_once 'clases/Persona.php';
require_once 'clases/Empresa.php';
require_once 'clases/Sucursal.php';

$identificacion = $_REQUEST['identificacion'];
$clave = $_REQUEST['clave'];
$persona = Persona::validar($identificacion, $clave);
if ($persona != null) {
    session_start();
    $_SESSION['usuario'] = $persona;
    $_SESSION['empresa'] = $persona->getEmpresaEnObjeto();
    $_SESSION['sucursal'] = $persona->getSucursalEnObjeto();
    

    header("Location: principal.php?contenido=empresa.php");
} else {
    header("location: index.php?mensaje=Acceso no autorizado.");
}